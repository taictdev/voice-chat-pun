﻿#if PUN_2_OR_NEWER

using System.Collections.Generic;
using System.Collections;
using CustomVoiceChat;
using Photon.Pun;
using Photon.Voice.PUN;
using Photon.Voice.Unity;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class ProximityVoiceTrigger : VoiceComponent
{
    private List<byte> groupsToAdd = new List<byte>();
    private List<byte> groupsToRemove = new List<byte>();

    [SerializeField] // TODO: make it readonly
    private byte[] subscribedGroups;

    private PhotonVoiceView photonVoiceView;
    private PhotonView photonView;
    private RoomController room;
    private Coroutine coroutine;

    public byte TargetInterestGroup
    {
        get
        {
            if (this.photonView != null)
            {
                return (byte)this.photonView.OwnerActorNr;
            }
            return 0;
        }
    }

    protected override void Awake()
    {
        this.photonVoiceView = this.GetComponentInParent<PhotonVoiceView>();
        this.photonView = this.GetComponentInParent<PhotonView>();
        Collider tmpCollider = this.GetComponent<Collider>();
        tmpCollider.isTrigger = true;
        this.IsLocalCheck();
    }

    private void ToggleTransmission()
    {
        if (this.photonVoiceView.RecorderInUse != null)
        {
            byte group = this.TargetInterestGroup;
            if (this.photonVoiceView.RecorderInUse.InterestGroup != group)
            {
                this.Logger.LogInfo("Setting RecorderInUse's InterestGroup to {0}", group);
                this.photonVoiceView.RecorderInUse.InterestGroup = group;
            }
            this.photonVoiceView.RecorderInUse.RecordingEnabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.IsLocalCheck() && other.CompareTag("GroupSpace"))
        {
            room = other.GetComponent<RoomController>();
            if (room != null)
            {
                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                    coroutine = null;
                }
                coroutine = StartCoroutine(RefreshUpdateEnterGroup());
            }
        }
    }

    private void UpdateEnterGroup()
    {
        List<byte> group = new List<byte>();
        group.AddRange(room.SubscribedGroups.ToArray());
        groupsToAdd.Clear();

        foreach (var item in group)
        {
            if (!this.groupsToAdd.Contains(item))
            {
                this.groupsToAdd.Add(item);
            }
        }

        if (!groupsToAdd.Contains(TargetInterestGroup))
        {
            groupsToAdd.Add(TargetInterestGroup);
        }

        // add player into room
        room.RPC_AddSubscribedGroups(TargetInterestGroup);

        if (subscribedGroups != null)
        {
            foreach (var item in subscribedGroups)
            {
                if (!this.groupsToRemove.Contains(item))
                {
                    this.groupsToRemove.Add(item);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (this.IsLocalCheck() && other.CompareTag("GroupSpace"))
        {
            Debug.Log("OnTriggerExit BEGIN");
            if (room != null)
            {
                List<byte> group = new List<byte>();
                group.AddRange(room.SubscribedGroups.ToArray());

                foreach (var item in group)
                {
                    if (this.groupsToAdd.Contains(item))
                    {
                        this.groupsToAdd.Remove(item);
                    }

                    if (!this.groupsToRemove.Contains(item))
                    {
                        this.groupsToRemove.Add(item);
                    }
                }

                room.RPC_RemoveSubscribedGroups(TargetInterestGroup);

                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                    coroutine = null;
                }

                Debug.Log("OnTriggerExit END");
            }
            else
            {
                Debug.Log($"OnTriggerExit room is null{room == null}");
            }
        }
    }

    private IEnumerator RefreshUpdateEnterGroup()
    {
        if (room != null)
        {
            UpdateEnterGroup();
        }
        yield return new WaitForSeconds(1);

        coroutine = StartCoroutine(RefreshUpdateEnterGroup());
    }

    protected void Update()
    {
        if (!PunVoiceClient.Instance.Client.InRoom)
        {
            this.subscribedGroups = null;
        }
        else if (this.IsLocalCheck())
        {
            if (this.groupsToAdd.Count > 0 || this.groupsToRemove.Count > 0)
            {
                byte[] toAdd = null;
                byte[] toRemove = null;
                if (this.groupsToAdd.Count > 0)
                {
                    toAdd = this.groupsToAdd.ToArray();
                }
                if (this.groupsToRemove.Count > 0)
                {
                    toRemove = this.groupsToRemove.ToArray();
                }
                this.Logger.LogInfo("client of actor number {0} trying to change groups, to_be_removed#={1} to_be_added#={2}", this.TargetInterestGroup, this.groupsToRemove.Count, this.groupsToAdd.Count);
                if (PunVoiceClient.Instance.Client.OpChangeGroups(toRemove, toAdd))
                {
                    if (this.subscribedGroups != null)
                    {
                        List<byte> list = new List<byte>();
                        for (int i = 0; i < this.subscribedGroups.Length; i++)
                        {
                            list.Add(this.subscribedGroups[i]);
                        }
                        for (int i = 0; i < this.groupsToRemove.Count; i++)
                        {
                            if (list.Contains(this.groupsToRemove[i]))
                            {
                                list.Remove(this.groupsToRemove[i]);
                            }
                        }
                        for (int i = 0; i < this.groupsToAdd.Count; i++)
                        {
                            if (!list.Contains(this.groupsToAdd[i]))
                            {
                                list.Add(this.groupsToAdd[i]);
                            }
                        }
                        this.subscribedGroups = list.ToArray();
                    }
                    else
                    {
                        this.subscribedGroups = toAdd;
                    }
                    this.groupsToAdd.Clear();
                    this.groupsToRemove.Clear();
                }
                else
                {
                    this.Logger.LogError("Error changing groups");
                }
            }
            this.ToggleTransmission();
        }
    }

    private bool IsLocalCheck()
    {
        if (this.photonView.IsMine)
        {
            return true;
        }
        if (this.enabled)
        {
            this.Logger.LogInfo("Disabling ProximityVoiceTrigger as does not belong to local player, actor number {0}", this.TargetInterestGroup);
            this.enabled = false;
        }
        return false;
    }

}
#endif