using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace CustomVoiceChat
{
    [RequireComponent(typeof(PhotonView))]
    public class RoomController : MonoBehaviourPun
    {
        [SerializeField]
        private List<byte> _subscribedGroups;

        public List<byte> SubscribedGroups { get => _subscribedGroups; private set => _subscribedGroups = value; }

        private void Start()
        {
            if (photonView.IsRoomView)
            {
                Debug.Log("This object is part of the PUN Room.");
            }
            else
            {
                Debug.Log("This object is not part of the PUN Room.");
            }
        }


        public void RPC_AddSubscribedGroups(byte group)
        {
            photonView.RPC("AddSubscribedGroups", RpcTarget.AllBuffered, group);
        }

        public void RPC_RemoveSubscribedGroups(byte group)
        {
            photonView.RPC("RemoveSubscribedGroups", RpcTarget.AllBuffered, group);
        }

        [PunRPC]
        private void AddSubscribedGroups(byte item)
        {
            if (!_subscribedGroups.Contains(item))
            {
                _subscribedGroups.Add(item);
            }
        }

        [PunRPC]
        private void RemoveSubscribedGroups(byte item)
        {
            if (_subscribedGroups.Contains(item))
            {
                _subscribedGroups.Remove(item);
            }
        }
    }
}
